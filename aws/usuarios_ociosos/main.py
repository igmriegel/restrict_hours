"""
    Arquivo de envio automático de e-mail para usuários ociosos conectados na
    EC2 em horários não permitidos.
    Script preparado para ser executado por crontab com a seguinte config:
    */30 * * * * - “At every 30th minute.”
"""
import os
import smtplib
import subprocess as sp
from datetime import date, datetime, time
from email.message import EmailMessage
from typing import Dict, List

import pytomlpp


def carregar_arquivo_config(config_file: str) -> Dict:
    """
    Função para carregar lib de parse do arquivo TOML.

    Parameters
    ----------
    config_file : str
        String com o caminho para o arquivo de configuração.

    Returns
    -------
    Dict
        Retorna um dicionário com as configurações convertidas para os tipos
    suportados pelo python.
    """
    return pytomlpp.load(config_file)


HOME = os.path.dirname(os.path.realpath(__file__))
ARQUIVO = HOME + "/config.toml"
CONFIG_FILE = carregar_arquivo_config(ARQUIVO)


def to_weekday(dias_semana: List[str]) -> List[int]:
    """
    Função responsável por converter nome de dia da semana em número
    :param dias_semana: lista de nomes de dias da semana
    :type dias_semana: List[str]
    :return: lista de dias da semana em numeral
    :rtype: List[int]
    """
    dias_map = {
        "seg": 1,
        "ter": 2,
        "qua": 3,
        "qui": 4,
        "sex": 5,
        "sab": 6,
        "dom": 7,
    }
    for dia in dias_semana:
        if dia not in ["seg", "ter", "qua", "qui", "sex", "sab", "dom"]:
            raise ValueError(f"[CONFIG] Dia de semana não é válido: {dia}")
    dias_semana = [dias_map[dia] for dia in dias_semana]
    return dias_semana


def dia_atual(lista_dias_trabalho: List[str]) -> list:
    """
    Função que gera informações de dia e horário com Datetime.
    :param lista_dias_trabalho: Lista de dias de trabalho preenchido no config.
    :type lista_dias_trabalho: List[str]
    :return: horário atual, dia corrente, e se é dia de trabalho ou descanso.
    :rtype: list
    """
    dia_corrente = date.today().isoweekday()
    dias_de_trabalho = to_weekday(lista_dias_trabalho)
    return [
        datetime.now().time(),
        date.today(),
        "trabalho" if dia_corrente in dias_de_trabalho else "descanso",
    ]


def reunir_info_contato_usrs(
    conectados: set,
    lista_de_emails: dict,
    lista_adms: list,
    users_ignorar: list,
) -> list:
    """_summary_

    Parameters
    ----------
    conectados : set
        _description_
    lista_de_emails : dict
        _description_
    lista_adms : list
        _description_
    users_ignorar : list
        _description_

    Returns
    -------
    list
        _description_
    """
    users_conectados = conectados.difference(set(users_ignorar))
    emails_adms = lista_adms
    online_users = {}

    for user in users_conectados:
        info_contato = (
            lista_de_emails.get(user)
            if lista_de_emails.get(user)
            else {"email": emails_adms, "nome": user}
        )
        online_users[user] = info_contato

    return online_users


def texto_email(
    dia: datetime,
    hora: time,
    tipo: str,
    user: str = "",
    emails_faltando: set = "",
) -> str:
    """
    Função que gera a string a ser enviada por email para os usuários.
    :param dia: Dia atual.
    :type dia: datetime
    :param hora: Hora corrente da execução do script.
    :type hora: time
    :param tipo: Tipo de mensagem a ser enviada [message_user, msg_user_adm,
    message_adm, faltando_adm]
    :type tipo: str
    :param user: Nome do usuário, defaults to ""
    :type user: str, optional
    :param ociosidade: Tempo de ociosidade registrado, defaults to ""
    :type ociosidade: str, optional
    :param emails_faltando: set de users administradores não registrados na
    lista de emails no config ini, defaults to ""
    :type emails_faltando: set, optional
    :return: Texto padrão do email
    :rtype: str
    """
    emails = {
        "message_user": f"""
        Olá {user}, você está logado hoje {dia.strftime("%d/%m/%Y")} às
        {hora.strftime("%H:%M")}, esse horário está definido como horário com
        restrição de acesso.\n
        Caso você esteja conectado e não esteja usando a maquina favor favor
        parar a instancia para evitar cobranças desnecessárias dos serviços da
        AWS.
        """,
        "msg_user_adm": f"""
        Olá Administrador, hoje {dia.strftime("%d/%m/%Y")} às
        {hora.strftime("%H:%M")} UTC±0 o {user} estava conectado no nosso
        servidor.
        Você está sendo avisado pois não temos o email de {user} na lista de
        emails da configuração.
        Favor desligar a maquina para evitar cobranças desnecessárias dos
        serviços da AWS.
        """,
        "message_adm": f"""
        Olá, hoje {dia.strftime("%d/%m/%Y")} às {hora.strftime("%H:%M")} UTC±0,
        nosso servidor está ligado sem nenhum usuário conectado.\n
        Favor desligar a maquina para evitar cobranças desnecessárias dos
        serviços da AWS.
        """,
        "faltando_adm": f"""Não possuímos o e-mail do administrador
        {''.join(emails_faltando)}.\n Favor inserir o contato do mesmo no
        arquivo config.toml""",
    }
    return emails[tipo]


def email_config_incorreta(
    dia: datetime, hora: time, config_invalida: str
) -> str:
    """
    Gera texto de email para configuração incorreta do config ini.
    :param dia: Dia corrente.
    :type dia: datetime
    :param hora: Hora da execução.
    :type hora: time
    :param config_invalida: Texto com a configuração incorreta e explicação de
    por que está incorreta.
    :type config_invalida: str
    :return: Texto padrão de email de erros no config ini.
    :rtype: str
    """
    texto = f"""Olá, hoje {dia.strftime("%d/%m/%Y")} as UTC±0 {hora.strftime("%H:%M")}
    o script não foi executado pois a seguinte configuração está incorreta:\n
    {config_invalida}"""
    return texto


def enviar_emails(remetente: str, senha: str, texto: str, dest: str) -> None:
    """
    Função que faz o envio dos e-mails.
    :param remetente: email do remetente
    :type remetente: str
    :param senha: senha para o cliente de envio do email
    :type senha: str
    :param texto: texto do email
    :type texto: str
    :param dest: destinatários dos emails
    :type dest: str
    """
    msg = EmailMessage()
    msg["Subject"] = "Usuário ocioso conectado no servidor AWS"
    msg["From"] = remetente
    msg["To"] = dest
    msg.add_header("Content-Type", "text/html")
    msg.set_payload(texto)

    send = smtplib.SMTP_SSL("smtp.gmail.com", 465)
    send.login(remetente, senha)
    send.sendmail(remetente, dest.split(","), msg.as_string().encode("utf-8"))
    send.quit()
    print(f"Email enviado para {dest.split(',')} ")


def valida_horario_comercial(
    dia: str, h_atual: time, restricao: List[int]
) -> bool:
    """
    Valida que a hora atual não está no range de horas restritas do config.ini
    as horas restritas são a lista de horas onde a instancia EC2 deveria estar
    desligada em dias normais. Adicionalmente considera também se o dia atual é
    dia útil ou de descanso (sabádos e domingos).
    :param dia: Tipo de dia, trabalho ou descanso
    :type dia: str
    :param h_atual: Hora corrente de execução do script
    :type h_atual: time
    :param restricao: Lista de horas onde a Beegol não tem expediente
    :type restricao: list[int]
    :return: Boleano resultado da validação
    :rtype: bool
    """
    horas_restritas = [int(h) for h in restricao]
    hora = int(str(h_atual)[0:2])

    if dia == "trabalho" and hora not in horas_restritas:
        return True

    return False


def main() -> None:
    """
    Função main de execução do script.
    :return: None
    :rtype: _type_
    """
    # pylint: disable=too-many-locals
    email_remetente = CONFIG_FILE["email_client"]["email_sender"]
    email_senha = CONFIG_FILE["email_client"]["email_password"]
    emails = CONFIG_FILE["contatos"]
    admins = CONFIG_FILE["admins"]["users"]
    ignorar = CONFIG_FILE["ignorar"]["usernames"]
    horas_restritas = CONFIG_FILE["configuracao"]["horas_restritas"]
    dias_uteis = CONFIG_FILE["configuracao"]["dias_uteis"]
    hora_atual, dia, tipo_dia = dia_atual(dias_uteis)
    adms_sem_email = set(admins).difference(set(emails.keys()))

    if adms_sem_email:
        admins = set(admins).intersection(set(emails.keys()))
        txt_adm_sem_email = texto_email(
            dia, hora_atual, "faltando_adm", emails_faltando=adms_sem_email
        )
        dest_adm = ", ".join([emails[adm]["email"] for adm in admins])
        enviar_emails(
            email_remetente, email_senha, txt_adm_sem_email, dest_adm
        )

    emails_adms = ", ".join([emails[adm]["email"] for adm in admins])

    if valida_horario_comercial(tipo_dia, hora_atual, horas_restritas):
        print(f"Nenhum e-mail enviado, {dia} as {hora_atual}.\n")
        return None

    lista_usrs_w_bash: list = sp.getoutput(
        "w -h | awk '{print $1}'| sort | uniq"
    ).split("\n")
    lista_usrs_top: list = sp.getoutput(
        "top -b -n1 | sed 1,7d | awk '{print $2}' | sort | uniq"
    ).split("\n")
    users_conectados = set(lista_usrs_w_bash + lista_usrs_top)

    if len(lista_usrs_w_bash) == 0:
        texto = texto_email(dia, hora_atual, "message_adm")
        enviar_emails(
            email_remetente, email_senha, texto, emails_adms,
        )
        print(f"e-mail enviado, {dia} as {hora_atual}, maquina sozinha.\n")
        return None

    email_ociosos = reunir_info_contato_usrs(
        users_conectados, emails, emails_adms, ignorar
    )

    for user, dados in email_ociosos.items():
        tipo_de_email = (
            "message_user" if user in list(emails.keys()) else "msg_user_adm"
        )
        dest = (
            f"{dados['email']}, {emails_adms}"
            if user in list(emails.keys())
            else emails_adms
        )
        texto = texto_email(
            user=user, dia=dia, hora=hora_atual, tipo=tipo_de_email,
        )
        enviar_emails(email_remetente, email_senha, texto, dest)

    print(f"{len(email_ociosos)} enviados, {dia} as {hora_atual}.\n")
    return None


if __name__ == "__main__":
    main()
